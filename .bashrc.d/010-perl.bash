# Perl Environment (Development, cpanm etc.)

# older configuration
# export PERL5LIB="/adzuna:/srv/adzuna/tools";
# export PERL_LOCAL_LIB_ROOT="${HOME}/perl5:${PERL_LOCAL_LIB_ROOT}";
# export PERL_MB_OPT="--install_base \"${HOME}/perl5\"";
# export PERL_MM_OPT="INSTALL_BASE=${HOME}/perl5";
# export PATH="${HOME}/perl5/bin:${PATH}";

# perlbrew
export PERLBREW_SKIP_INIT=1
export PERLBREW_VERSION=0.87
export PERLBREW_HOME=/opt/perlbrew
export PERLBREW_ROOT=/opt/perlbrew
export PERLBREW_PERL=perl-5.26.1
export PERLBREW_MANPATH=/opt/perlbrew/perls/perl-5.26.1/man
export PERLBREW_PATH=/opt/perlbrew/bin:/opt/perlbrew/perls/perl-5.26.1/bin
export PERLBREW_LIB=

# perl
export PERL5LIB="/adzuna:/srv/adzuna/tools"
export PERL_LOCAL_LIB_ROOT=

# paths
export MANPATH=/opt/perlbrew/perls/perl-5.26.1/man:
export PATH=/opt/perlbrew/bin:/opt/perlbrew/perls/perl-5.26.1/bin:$PATH

# linker
export LD_LIBRARY_PATH=/opt/perlbrew/perls/perl-5.26.1/local/lib/
