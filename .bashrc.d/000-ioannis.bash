### ioannis custom directives

export LC_ALL="en_US.UTF-8";
export LC_CTYPE="en_US.UTF-8";

if [ -d ~/bin ]; then
    export PATH="~/bin:$PATH"
fi

if [ -d ~/work/bin ]; then
    export PATH="~/work/bin:${PATH}"
fi


# make user's tmp dir
if [ ! -d /tmp/user/$USER ]; then
    mkdir -p /tmp/user/$USER
fi

if [ ! -e $HOME/tmp ]; then
    ln -s /tmp/user/$USER $HOME/tmp
fi

export PROMPT="\[\e[32m\]\u\[\e[m\]@\[\e[32m\]\h\[\e[m\]:\[\e[36m\]\w\[\e[m\]\n$ " # compatible with bash-it
export PS1="$PROMPT"

alias vi='vim'
alias view="vim -M"
alias date='\date +"%Y-%m-%d %H:%M:%S"'
alias codedate='\date +"%Y%m%d%H%M%S"'

function psg {
    ps -ef |grep "$@";
}

function plfind {
    find $1 | grep -P $2;
}

function plgrep {
    local code="while (<STDIN>) { print if qr_${1}_; }"
    perl -e "$code"
}

function mkcd {
    mkdir -p $1 && cd $1
}

function openport {
    sudo iptables -I INPUT 1 -p tcp -m tcp --dport $1 -j ACCEPT
}

function pmcheck {
    git diff --name-only --diff-filter=ACRM master... | grep -P '\.p[lm]' |xargs -r -I{} sh -c 'perlcritic -p /adzuna/perlcriticrc {} || true';
}

alias ls='ls --color=auto -a'

# GIT_PS1=$HOME/bin/git-ps1
# if [ -f ${GIT_PS1} ]; then
#   export PS1='$(${GIT_PS1} "[\u@\h \W]\$ ")'
# fi

export VISUAL=vim
export EDITOR="$VISUAL"