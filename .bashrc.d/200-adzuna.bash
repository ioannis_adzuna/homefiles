# Based on https://adzira.atlassian.net/browse/INF-329
function perl-base {
    # if argument is not set, revert to default
    if [ -z "$1" ]; then
        # deactivate all local libs
        eval `perl -Mlocal::lib=--deactivate-all`
        echo "Deactivated Perl local lib; back to default..."
        return 0;
    fi
    # if argument is set, create and use it as name for the local lib
    name=$1;
    basedir="$HOME/perl5/$name";
    # prepare the directory if needed
    if [ ! -d "$basedir" ]; then
        mkdir -p "$basedir";
    fi
    # set perl local lib directory
    eval $(perl -Mlocal::lib=$basedir);
    echo "Current Perl local lib: $basedir";
    return 0;
}



# common aliases

alias mysql='mysql -uroot -prootpass adzuna_core'

alias simple-http-server='python -m SimpleHTTPServer'

# scraping aliases

alias abstract-tokenizer='perl scripts/ad_hoc/tokeniser/test_abstract_tokenizer.pl';

alias create-deploy-config='perl /srv/adzuna/public/scripts/ad_hoc/create-deploy-config.pl'

alias create-validation-report='perl /srv/adzuna/tools/bin/create_validation_report.pl'

alias deploy-site='perl scripts/scraping/deploy.pl'

## JS scraping

alias kill_ff='sudo killall firefox; sudo killall geckodriver; sudo rm -f /tmp/*firefox*lock'

alias ps_ff='ps ax | egrep "firefox|geckodriver"'

alias psef_ff='ps -ef | egrep "firefox|geckodriver"'

function mock-scrape {
    kill_ff;
    adzuna-clear-all;
    perl \
        -MAdzTest::Mock::Scrape \
        -e '$scrape=AdzTest::Mock::Scrape->new(source=>'$1'); $scrape->show_events(1); $scrape->run();' \
    2>&1 |tee /var/log/adzuna/mock-scrape.log
}

# alias tokenize2='perl scripts/scraping/browser/tokenize.pl'
function tokenize {
    kill_ff;
    perl scripts/scraping/browser/tokenize.pl "$@" 2>&1 |tee /var/log/adzuna/tokenize.log;
}

function rebase-log {
    branch=$(git branch | grep \* | cut -d ' ' -f2);
    adzuna-git-log $branch master;
}

alias scrape-site='perl /adzuna/scripts/ad_hoc/scrape_site.pl'

function js-scrape {
    kill_ff;
    adzuna-clear-all;
    perl -MAdzTest::Mock::Scrape \
        -e "\$scrape=AdzTest::Mock::Scrape->new(source=>\"$1\"); \$scrape->show_events(1); \$scrape->run();" \
        2>&1 |tee /var/log/adzuna/js-scrape.log;
}

alias validate-yaml='perl scripts/scraping/testing/validate-yaml.pl --all-tests --conf'

function reset-adzlog {
    cd /var/log/adzuna;
    cp adzuna_debug.log adzuna_debug.log.$(\date +"%Y%m%d%H%M%S");
    echo -n > adzuna_debug.log;
    cd -;
}

alias view-adzlog='less /var/log/adzuna/adzuna_debug.log'

## logo

alias make-logo='perl /adzuna/scripts/ad_hoc/graphics/make_logo.pl'

function fix-logo-border {
    convert $1 -matte -bordercolor none -border 4x4 $1
}

# database aliases

function locate_db_constraint {
    local query="
        USE INFORMATION_SCHEMA;
        SELECT TABLE_NAME,
            COLUMN_NAME,
            CONSTRAINT_NAME,
            REFERENCED_TABLE_NAME,
            REFERENCED_COLUMN_NAME
        FROM KEY_COLUMN_USAGE
        WHERE REFERENCED_COLUMN_NAME IS NOT NULL
            AND CONSTRAINT_NAME LIKE '%$1%'
    ";
    echo $query | mysql |column -t -x
}

alias update_db_sources='bash /srv/adzuna/public/scripts/scraping/update_db.sh'

# go to public project by default

cd /srv/adzuna/public
