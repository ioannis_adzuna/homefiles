SELECT
    CONCAT_WS('%%%',
        'Site ID',
        'Site Name',
        'Source ID',
        'Source name',
        'Freq',
        'Source boost',
        'App boost',
        'Source partner',
        'Source PPC',
        'Campaign status',
        'Direct link',
        'Live ads'
    )
UNION
SELECT
    CONCAT_WS('%%%',
        COALESCE(`Site`.id, ''),
        COALESCE(`Site`.name, ''),
        COALESCE(UkSource.id, ''),
        COALESCE(UkSource.description, ''),
        COALESCE(UkSource.frequency_minutes, ''),
        COALESCE(UkSource.boost, ''),
        COALESCE(UkSource.app_boost, ''),
        COALESCE(UkSource.partner_status, ''),
        COALESCE(UkSource.ppc, ''),
        COALESCE(UkSource.campaign_status, ''),
        COALESCE(UkSource.is_direct_link, ''),
        COALESCE(AdCount.total, 0)
    )
FROM
    Site
    LEFT JOIN (
        -- UK Sources with campaign status
        SELECT
            Source.id AS 'id',
            Source.site_id AS 'site_id',
            Source.description AS 'description',
            Source.frequency_minutes AS 'frequency_minutes',
            Source.boost AS 'boost',
            Source.app_boost AS 'app_boost',
            Source.partner_status AS 'partner_status',
            Source.ppc AS 'ppc',
            Campaign.status AS 'campaign_status',
            is_direct_link AS 'is_direct_link'
        FROM
            Source
            LEFT JOIN Campaign
            ON Source.campaign_id = Campaign.id
        WHERE
            Source.context = 'jobs'
    )
    UkSource
    ON UkSource.site_id = Site.id
    LEFT JOIN (
        -- Count ads per source before joining
        SELECT
            AdMaster.source_id AS 'source_id',
            COUNT(AdMaster.id) AS 'total'
        FROM
            AdMaster,
            Source
        WHERE
            -- join ads to sources to filter by context
            -- and avoid comparing every source with the outer sources
            AdMaster.source_id = Source.id AND
            Source.context = 'jobs' AND
            AdMaster.status_id <> 0
        GROUP BY
            AdMaster.source_id
    )
    AdCount
    ON AdCount.source_id = UkSource.id
;
