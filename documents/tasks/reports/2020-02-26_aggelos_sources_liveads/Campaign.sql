CREATE TABLE `Campaign` (
    `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    `crm_id` bigint(20) unsigned DEFAULT NULL COMMENT 'IDs returned by the CRM are on the order of Zilion',
    `name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `budget` decimal(9, 2) DEFAULT NULL,
    `cpc` decimal(6, 4) DEFAULT NULL,
    `created` datetime DEFAULT '0000-00-00 00:00:00',
    `currency_id` smallint(3) unsigned NOT NULL,
    `starts` datetime NOT NULL,
    `ends` datetime NOT NULL,
    `client_id` mediumint(8) unsigned DEFAULT '0',
    `crm_data` mediumtext COLLATE utf8mb4_unicode_ci,
    `cancelled` tinyint(4) NOT NULL DEFAULT '0',
    `status` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Closed Won',
    PRIMARY KEY (`id`),
    KEY `fk_currency_id_idx` (`currency_id`),
    CONSTRAINT `_fk_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `Currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPRESSED
