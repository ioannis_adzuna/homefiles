CREATE TABLE `AdMaster` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `class` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'type of ad (jobs, property, automoto, etc)\n',
    `source_id` mediumint(8) unsigned NOT NULL,
    `created_source` datetime NOT NULL,
    `created_here` datetime DEFAULT NULL,
    `modified_here` datetime DEFAULT NULL,
    `source_ref` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'unique identifier used by source\n',
    `status_id` int(11) DEFAULT '1',
    `digest_id` bigint(20) unsigned NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `fk_ads_2` (`source_id`, `source_ref`),
    KEY `fk_ads_1` (`source_id`),
    KEY `idx_status_id` (`status_id`),
    CONSTRAINT `fk_ads_1` FOREIGN KEY (`source_id`) REFERENCES `Source` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1008544 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci
