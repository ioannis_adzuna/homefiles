CREATE TABLE `Site` (
    `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
    `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 7186 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci
