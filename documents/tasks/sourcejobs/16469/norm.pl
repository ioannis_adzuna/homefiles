#/usr/bin/env perl

use strict;
use warnings;

use Encode qw(decode);
use I18N::Langinfo qw(langinfo CODESET);

use Container;
use Context;
use Functions::Normalizer;

my $codeset = langinfo(CODESET);

binmode STDOUT, ":encoding($codeset)";


my $value = decode($codeset, shift);
my $attribute = shift || 'location';
my $context = shift || 'jobs_US';

Context->current->set_context($context);

my $normalizer = Functions::Normalizer->new();

my $attribute_method = "normalize_$attribute";

my $result = $normalizer->$attribute_method(field => $attribute, values => { $attribute => $value, geo_lat => "", geo_lng => "", geo_inferred => "" } );

print Dumper_JSON $result;
