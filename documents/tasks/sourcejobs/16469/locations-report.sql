SELECT
    adfv.value AS 'Location Raw',
    adfi.value AS 'Location ID',
    if (adfi.value = 136073, 1, 0) AS 'Defaulted'
FROM
    AdMaster AS ad,
    AdFieldInt AS adfi,
    AdFieldVarchar AS adfv,
    Source AS src
WHERE
    -- joins
    ad.id = adfi.ad_id AND
    ad.id = adfv.ad_id AND
    ad.source_id = src.id AND
    -- filters
    adfi.field = 'location_id' AND
    adfv.field = 'location_raw' AND
    src.site_id = 6239 AND
    1 = 1
