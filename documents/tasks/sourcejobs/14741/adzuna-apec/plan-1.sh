#!/bin/bash

# TODO
# At Step 2:
#  - curl: (56) SSL read: error:00000000:lib(0):func(0):reason(0), errno 104


# Include common definitions
source $(dirname $0)/_common.sh


_step_title '1. Get authorization token';

_curl \
-X POST \
-H 'Content-Length: 0' \
"https://data.apec.fr/rest/auth/Begin?clientName=WINCLIENT&Root=1&userName=EUCLIDE-APEC%5C${partner_identifier}" \
;

authorization_token=$(jq '.Data.AuthToken' ${_content_file});
authorization_token=$(echo ${authorization_token} | tr -d \");

_report_step;



_step_title '2. Authenticate';

_body="{ \"InputParam\": \"${partner_pass}\", \"RootID\": 1 }";
_length=$(echo "${_body}" | wc -c);

_curl \
-X POST \
-H "AuthorizationToken: ${authorization_token}" \
-H "Content-Type: application/json" \
-H "Content-Length: ${_length}" \
'https://data.apec.fr/rest/auth/V2/Continue' \
--data "${_body}" \
;

_report_step;



_step_title '3. Identify the WorkspaceID';

_body='{ "AcceptedReceivedPublicWorkspace": true }';
_length=$(echo "${_body}" |wc -c);

_curl \
'https://data.apec.fr/rest/client/V3/Workspace/All' \
-X POST \
-H "AuthorizationToken: ${authorization_token}" \
-H "Content-Type: application/json" \
-H "Content-Length: ${_length}" \
--data "${_body}" \
;

workspace_id=$(jq '.Data.PublicWorkspacesList[0].WorkspaceID' ${_content_file});

_report_step;



_step_title '4. Identify the RootID from WorkspaceID';

_body='{ "WorkspaceItemsList": true, "IncludeOwnerData": false }';
_length=$(echo "${_body}" |wc -c);

_curl \
"https://data.apec.fr/rest/client/V3/Workspace?ID=${workspace_id}" \
-X POST \
-H "AuthorizationToken: ${authorization_token}" \
-H "Content-Type: application/json" \
-H "Content-Length: ${_length}" \
--data "${_body}" \
;

path=$(jq '.Data.ListOfItems[0].Path' ${_content_file});
path=$(echo ${path} | tr -d \");
root_id=$(jq '.Data.ListOfItems[0].RootID' ${_content_file});

_report_step;



_step_title '5. Identify the file using partner identifier, path and RootID';

# no body for this step

_curl \
"https://data.apec.fr/rest/transfer/download/Query?type=1&revision=0&root=${root_id}&path=${path}%5C${partner_identifier}%5CExport_offres_${partner_identifier}.xml" \
-X POST \
-H "AuthorizationToken: ${authorization_token}" \
;

download_size=$(jq '.Data.DownloadSize' ${_content_file});
download_id=$(jq '.Data.ID' ${_content_file});
download_id=$(echo $download_id | tr -d \");

_report_step;



_step_title '6. Download the file';

_curl \
"https://data.apec.fr/rest/transfer/Download/StartDownloadFile?root=${root_id}&offset=0&size=${download_size}&downloadID=${download_id}" \
-X GET \
-H "AuthorizationToken: ${authorization_token}" \
;

_report_step;


_cleanup;
