#!/bin/bash

# set -x

# Common definitions
partner_identifier='adzuna';
partner_identifier_upper='ADZUNA';
partner_pass='A8LM_h6j';
user_agent='Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0';
curl=`which curl`;


_header_file=$(mktemp);
_content_file=$(mktemp);


function _curl {
    $curl \
        --location \
        --dump-header ${_header_file} \
        --output ${_content_file} \
        --silent \
        --show-error \
        "$@";
}


function _cleanup {
    rm ${_header_file};
    rm ${_content_file};
}


function _press_key {
    read -n 1 -s -r -p "Press any key to continue";
}


function _step_title {
    echo;
    echo;
    echo "$1";
    echo;
}


function _report_env {
    echo user_agent = ${user_agent};
    echo partner_identifier = ${partner_identifier};
    echo authorization_token = ${authorization_token};
    echo workspace_id = ${workspace_id};
    echo path = ${path};
    echo root_id = ${root_id};
    echo download_size = ${download_size};
    echo download_id = ${download_id};

    echo;
}


function _report_step {
    cat ${_header_file} ${_content_file};
    echo;
    echo;
    _report_env;
    _press_key;
}


function _url_encode {
    perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$@"
}


# Code here

_step_title '1. Login';

_curl \
-X POST \
-H 'Content-Length: 0' \
"https://data.apec.fr/rest/auth/Begin?clientName=WINCLIENT&Root=1&userName=EUCLIDE-APEC\\${partner_identifier}" \
;

authorization_token=$(jq '.Data.AuthToken' ${_content_file});
authorization_token=$(echo ${authorization_token} | tr -d \");

_report_step;



_step_title '2. Password';

_body="{ \"InputParam\": \"${partner_pass}\", \"RootID\": 1 }";

_curl \
-X POST \
-H "AuthorizationToken: ${authorization_token}" \
-H "Content-Type: application/json" \
'https://data.apec.fr/rest/auth/V2/Continue' \
--data "${_body}" \
;

_report_step;



_step_title '3. Get downloadID for the file';

# no body for this step

_curl \
"https://data.apec.fr/rest/transfer/download/Query?type=1&revision=0&root=8&path=IT\\IP\\PARTENAIRES\\${partner_identifier_upper}\\Export_offres_${partner_identifier_upper}.xml" \
-X POST \
-H "AuthorizationToken: ${authorization_token}" \
-H "Content-Length: 0" \
;

download_id=$(jq '.Data.ID' ${_content_file});
download_id=$(echo $download_id| tr -d \");

_report_step;



_step_title '4. Download the file --';

# no body for this step

start_time=$(date +%s);
_curl \
"https://data.apec.fr/rest/transfer/Download/DownloadFileAfterQuery?file=Export_offres_${partner_identifier_upper}.xml&root=8&downloadID=${download_id}" \
-X GET \
-H "AuthorizationToken: ${authorization_token}" \
-H "Content-Type: application/xml" \
;
end_time=$(date +%s);


runtime=$((end_time-start_time));
echo Took: $runtime;
cat ${_header_file};

cp $_content_file /tmp/ioannis/apec-plan-2.feed



_cleanup;
