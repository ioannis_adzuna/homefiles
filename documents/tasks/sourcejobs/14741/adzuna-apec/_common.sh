partner_identifier='adzuna';
partner_identifier_upper='ADZUNA';
partner_pass='A8LM_h6j';
user_agent='Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0';
curl=`which curl`;


_header_file=$(mktemp);
_content_file=$(mktemp);


function _curl {
    $curl \
        --location \
        --dump-header ${_header_file} \
        --output ${_content_file} \
        --silent \
        --show-error \
        "$@";
}


function _cleanup {
    rm ${_header_file};
    rm ${_content_file};
}


function _press_key {
    read -n 1 -s -r -p "Press any key to continue";
}


function _step_title {
    echo;
    echo;
    echo "$1";
    echo;
}


function _report_env {
    echo user_agent = ${user_agent};
    echo partner_identifier = ${partner_identifier};
    echo authorization_token = ${authorization_token};
    echo workspace_id = ${workspace_id};
    echo path = ${path};
    echo root_id = ${root_id};
    echo download_size = ${download_size};
    echo download_id = ${download_id};

    echo;
}


function _report_step {
    cat ${_header_file} ${_content_file};
    echo;
    echo;
    _report_env;
    _press_key;
}
