
cd /adzuna

# Install site in the database
# If updating from html -> xml, just deactivate the old source (set freq to 0)
mysql adzuna_core -uroot -p < /adzuna/scripts/new_sources_import/jobs/fr/import_groupeassu2000XML.sql

# XML site: Blacklist all proxies, whitelist __NO_PROXY__
perl -MContainer -MDomain::Site -e 'Domain::Site->get( 000 )->blacklist_proxies;'
perl -MContainer -MDomain::Site -e 'Domain::Site->get( 000 )->whitelist_proxies( 183 );'
# OR
# perl -e 'use Container; use Domain::Site; $s = Domain::Site->get( 000 ); $c = $s->blacklist_proxies;'
# perl -e 'use Container; use Domain::Site; $s = Domain::Site->get( 000 ); $c = $s->whitelist_proxies( 183 );'

# Activate all site sources, set frequency_minutes = 1440 (default)
perl scripts/ad_hoc/set_site_properties.pl --site 000 --freq 1440
# OR
perl -MContainer -MDomain::Source -e 'my @sources = Domain::Source->find( {site_id => { "=" => 000 }} ); foreach (@sources) { $_->set_field("frequency_minutes", 1440); $_->update; }'
# OR directly:
echo 'update Source set frequency_minutes=1440 where site_id=000;' | mysql adzuna_core -u root -p
