#!/usr/bin/perl

use Adzuna::DSL::YAMLDeploy;

site(
    name => 'Hello Handicap',
    logo => 'images/fr/jobs/partners/logo_hellohandicap.png',
    proxies => 'localhost',
);

build_currency_split(
    filter => {
        tokenizer =>
                { country => 'adzuna:location:fr' },
    },
    description_format => 'Hello Handicap: XML all ads %s',
    pool_name => 'hellohandicap-cpc-fr',
    attribute => 'cpc',
    conf => 'scraper/fr/www/job/hellohandicap.conf',
    split_values => [0, 0.10, 0.12, currency_range(0.15, 1.00, 0.05)],
    context => 'jobs_FR',
    sales => 'Direct',
    category => 'unknown',
    url => 'https://jobfeed.goldenbees.fr/?t=2ns6zB3Xq34H4LyD9804a8d6218a63771473c46e3bee635d9ef244a9&cid=404&format=adzuna',
    frequency_minutes => 360,
);

output;
