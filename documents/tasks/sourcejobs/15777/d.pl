#!/usr/bin/env perl

use strict;
use warnings;

use Adzuna::DSL::YAMLDeploy;

existing_site(id=>751);

for my $src_id (@{sources_of_site(id=>751)})
{
    if ( $src_id == 17738 ) {
        existing_source(
            id => $src_id,
            url => 'https://files.channable.com/uPV4wU9t9H8K4TReDwrxyA==.xml',
        );
    }
    else {
        existing_source( id => $src_id );
    }
}

build_currency_split(

    category => 'unknown',
    conf => 'scraper/nl/www/job/nationalevacaturebank.conf',
    context => 'jobs_NL',
    description_format => 'Nationale Vacature Bank NL: XML all ads - Extra - cpc %s',
    frequency_minutes => '1440',
    url => 'https://files.channable.com/akzwxHgeNsyhXuvDjaj3hg==.xml',
    sales => 'Job board',

    attribute => 'cpc',
    split_values => [0, currency_range(0.12, 1.00)],
    pool_name => 'nationalvacaturebank-extra-cpc-nl',

);


output;
