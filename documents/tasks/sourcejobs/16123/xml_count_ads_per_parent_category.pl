#!/usr/bin/env perl

use strict;
use warnings;

use XML::Simple;


binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");


my $xmlfile = shift @ARGV;

my $perldoc = XMLin($xmlfile);

my $ad_index = 0;

my %ads_per_category;

for my $job (@{$perldoc->{'Jobs'}})
{
    my $categories = $job->{'Categories'};

    if ( ref($categories) eq 'ARRAY' )
    {
        $ads_per_category{$categories->[0]->{'ParentName'}} ||= 0;
        $ads_per_category{$categories->[0]->{'ParentName'}} += 1;
    }
    elsif ( ref($categories) eq 'HASH' )
    {
        $ads_per_category{$categories->{'ParentName'}} ||= 0;
        $ads_per_category{$categories->{'ParentName'}} += 1;
    }
    else
    {
        $ads_per_category{'OTHER'} ||= 0;
        $ads_per_category{'OTHER'} += 1;
    }

    $ad_index++;
}

####

my $category_count = 0;
my $total_ads  = 0;

foreach my $category (keys(%ads_per_category))
{
    my $ad_count = $ads_per_category{$category};
    $category_count++;
    $total_ads += $ad_count;
    printf "\"%s\",%d\n", $category, $ad_count;
}

printf STDERR "Total ads: %0.2f\n", $total_ads;
printf STDERR "Total (reduced) categories: %0.2f\n", $category_count;
printf STDERR "Average ads per parent category: %0.2f\n", ($total_ads/$category_count);


exit 0;
