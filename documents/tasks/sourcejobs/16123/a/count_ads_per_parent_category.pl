#!/usr/bin/env perl

use strict;
use warnings;

use YAML::XS qw();


binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");


my $jsonfile = shift @ARGV;

my $perldoc = YAML::XS::LoadFile($jsonfile);

my $ad_index = 0;

my %ads_per_category;

for my $job (@{$perldoc->{'Jobs'}})
{
    my $categories = $job->{'Categories'};

    if ( ref($categories) ne 'ARRAY' )
    {
        printf STDERR "Ad #%d: categories type %s\n", $ad_index, ref($categories);
    }
    else
    {
        for my $category (@$categories)
        {
            $ads_per_category{$category->{'ParentName'}} ||= 0;
            $ads_per_category{$category->{'ParentName'}} += 1;
        }
    }

    $ad_index++;
}

####

my $category_count = 0;
my $total_ads  = 0;

foreach my $category (keys(%ads_per_category))
{
    my $ad_count = $ads_per_category{$category};
    $category_count++;
    $total_ads += $ad_count;
    printf "\"%s\",%d\n", $category, $ad_count;
}

printf STDERR "Average ads per parent category: %0.2f\n", ($total_ads/$category_count);


exit 0;
