#!/usr/bin/env perl

use strict;
use warnings;

use Adzuna::DSL::YAMLDeploy;

existing_site(id=>771);

for my $src_id (@{sources_of_site(id=>751)})
{
    if ( $src_id == 161701 ) {
        existing_source(
            id => $src_id,
            url => 'https://files.channable.com/uPV4wU9t9H8K4TReDwrxyA==.xml',
        );
    }
    else {
        existing_source( id => $src_id );
    }
}

build_split(

    description_format => 'pracuj.pl: XML API all ads %s',
    pool_name => 'pracujapi-category-pl',
    attribute => ''

);


output;
