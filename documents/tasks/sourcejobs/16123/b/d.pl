#!/usr/bin/env perl

use Adzuna::DSL::YAMLDeploy;

use utf8;

my $categories = [
    "Administracja biurowa",
    "BHP / Ochrona środowiska",
    "Badania i rozwój",
    "Bankowość",
    "Budownictwo",
    "Call Center",
    "Doradztwo / Konsulting",
    "Edukacja / Szkolenia",
    "Energetyka",
    "Finanse / Ekonomia",
    "Franczyza / Własny biznes",
    "Hotelarstwo / Gastronomia / Turystyka",
    "Human Resources / Zasoby ludzkie",
    "IT - Administracja",
    "IT - Rozwój oprogramowania",
    "Inne",
    "Internet / e-Commerce / Nowe media",
    "Inżynieria",
    "Kontrola jakości",
    "Marketing",
    "Media / Sztuka / Rozrywka",
    "Nieruchomości",
    "Obsługa klienta",
    "Praca fizyczna",
    "Prawo",
    "Produkcja",
    "Public Relations",
    "Reklama / Grafika / Kreacja / Fotografia",
    "Sektor publiczny",
    "Sprzedaż",
    "Transport / Spedycja / Logistyka",
    "Ubezpieczenia",
    "Zakupy",
    "Zdrowie / Uroda / Rekreacja",
    "Łańcuch dostaw",
];

my $reused_sources = [161701, (15272..15306), 17305];


existing_site(
    id => 771,
);

for my $src_id (@{sources_of_site(id=>771)})
{
    next if (grep {$_ == $src_id} @$reused_sources);
    existing_source(id => $src_id);
}

build_split(

    reuse_sources => $reused_sources,
    sort_reused_sources => 0,

    category => 'unknown',
    conf => 'scraper/pl/www/job/pracuj_api.conf',
    context => 'jobs_PL',
    frequency_minutes => '360',
    url => 'https://accounts.pracuj.pl/connect/token',
    sales => 'Job board',

    description_format => 'pracuj.pl: XML API all ads %s',

    pool_name => 'pracuj-parentcategory-pl',
    attribute => 'Categories/ParentName',
    split_values => $categories,

    filter => undef,


);

output;
