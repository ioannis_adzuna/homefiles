#!/usr/bin/env perl

use strict;
use warnings;
use utf8;


use Data::Dumper;
use List::MoreUtils qw(uniq);
use File::Slurp qw();
use JSON::XS qw();


my $json_text = File::Slurp::read_file($ARGV[0]);

my $perl_obj = JSON::XS->new->utf8(0)->decode($json_text);

my @jobs = @{$perl_obj->{'Jobs'}};

my @result;

for my $job (@jobs)
{
    my $category = $job->{'Categories'};
    if (ref($category) eq 'ARRAY')
    {
        for my $c (@$category)
        {
            push @result, $c->{'ParentName'};
        }
    }
    elsif (ref($category) eq 'HASH')
    {
        push @result, $category->{'ParentName'};
    }
    else
    {
        print STDERR "Unexpected ref type for: " . Dumper($category);
    }
}

for my $line (sort(uniq(@result)))
{
    print $line, "\n";
}


exit 0;
