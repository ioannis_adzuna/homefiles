#!/usr/bin/env perl

use Adzuna::DSL::YAMLDeploy;

existing_site(id=>5230);

my $split_values = [0, 0.10, 0.12, currency_range(0.15,1.00,0.05)];

build_currency_split(

    category => 'engineering-jobs',
    conf => 'scraper/fr/www/job/sncf.conf',
    context => 'jobs_FR',
    description_format => 'SNCF-Technicien: XML all ads - cpc %s',
    frequency_minutes => '360',
    url => 'https://jobfeed.goldenbees.fr/?t=2ns6zB3Xq34H4LyD9804a8d6218a63771473c46e3bee635d9ef244a9&cid=244&format=adzuna',
    sales => 'Direct',

    attribute => 'cpc',
    split_values => $split_values,
    pool_name => 'sncf-technicien-cpc-fr',

);

output;
