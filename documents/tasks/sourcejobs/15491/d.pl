use Adzuna::DSL::YAMLDeploy;

existing_site_with_sources(
    id => 3438
);


build_currency_split(

    build_fetcher => 0,
    build_other => 0,

    pool_name => 'cadreo-cpc-fr',

    category => 'unknown',
    conf => 'scraper/fr/www/job/cadreo2.conf',
    context => 'jobs_FR',

    description_format => 'Cadreo: XML all ads %s',

    attribute => 'cpc',
    split_values => [currency_range(0.21, 1, 0.01) ],

    url => 'https://api.hrtraxx.com/XmlDownload/GetXml?tokenClient=2ead3cb0f670423c9496bdf131423ec0&clientFlux=Adzuna&campaign=cadreo',
    sales => 'Job board',

    split_regex_map => sub{ $_[0] =~ s/\./,/s; 'qr~'.quotemeta($_[0]).'~si' },

);

output;
