echo 'cd /srv/adzuna/source_logos;' > /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
echo 'git pull;' >> /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
perl scripts/ad_hoc/graphics/make_logo.pl -i https://www.placementpartner.co.za/client_data/hwseta/company.jpg -o logo_hwseta.png --cc za --jira SOURCEJOBS-15380 --name 'HWSETA' --cmd-file /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
perl scripts/ad_hoc/graphics/make_logo.pl -i https://www.placementpartner.co.za/client_data/iwcp/company.jpg -o logo_iwcp.png --cc za --jira SOURCEJOBS-15380 --name 'IWCP' --cmd-file /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
perl scripts/ad_hoc/graphics/make_logo.pl -i https://www.placementpartner.co.za/client_data/studentvillage/company.jpg -o logo_studentvillage.png --cc za --jira SOURCEJOBS-15380 --name 'Student Village' --cmd-file /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
perl scripts/ad_hoc/graphics/make_logo.pl -i https://www.placementpartner.co.za/client_data/purplepanda/company.jpg -o logo_purplepanda.png --cc za --jira SOURCEJOBS-15380 --name 'Purple Panda Placements' --cmd-file /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
perl scripts/ad_hoc/graphics/make_logo.pl -i https://www.placementpartner.co.za/client_data/peopable/company.jpg -o logo_peopable.png --cc za --jira SOURCEJOBS-15380 --name 'Peopable Recruitment' --cmd-file /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
echo 'git push origin master;' >> /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
echo 'cd -;' >> /srv/adzuna/public/scripts/scraping/placement-partner/push-logos.sh
