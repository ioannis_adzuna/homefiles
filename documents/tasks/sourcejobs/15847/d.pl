#!/usr/bin/env perl

use Adzuna::DSL::YAMLDeploy;

scrape_type('XML');

site(
    logo => 'images/fr/jobs/partners/logo_groupeassu2000.png',
    name => 'Groupe Assu 2000',
    proxies => 'localhost',
);

# build split sources according to issues specification and usual practices
build_currency_split(


    category => 'unknown',
    conf => 'scraper/fr/www/job/groupeassu2000.conf',
    context => 'jobs_FR',
    frequency_minutes => '360',
    url => 'https://jobfeed.goldenbees.fr/?t=2ns6zB3Xq34H4LyD9804a8d6218a63771473c46e3bee635d9ef244a9&cid=396&format=adzuna',
    sales => 'Direct',

    description_format => 'Groupe Assu 2000: XML all ads %s',

    pool_name => 'groupeassu2000-fr-cpc',
    attribute => 'cpc',
    split_values => [ 0, 0.10, 0.12, currency_range(0.15, 1, 0.05) ],

    filter => { tokenizer => { country => 'adzuna:location:fr' } },


);

output;
