#!/usr/bin/env perl

use Adzuna::DSL::YAMLDeploy;


site(
    logo => 'images/us/jobs/partners/logo_veyo.png',
    name => 'Veyo',
    proxies => 'localhost',
);

build_currency_split(

    category => 'logistics-warehouse-jobs',
    conf => 'scraper/us/www/job/veyo.conf',
    context => 'jobs_US',
    description_format => 'Veyo: XML all ads %s',
    frequency_minutes => '360',
    url => 'https://clickcastfeeds.s3.amazonaws.com/867bec450671f3817e911e721231a196/3ce51f2ac09a0bee55887d66f58abac8.xml.gz',
    sales => 'Agency',

    attribute => 'cpc',
    split_values => [0, currency_range(0.20, 1.50)],
    pool_name => 'veyo-cpc-us',

);

output;
