#!/bin/bash

# set -x

# Common definitions
curl=`which curl`;
api_host='www.pracuj.pl'
client_id='adzuna';
client_secret='M6cgD7LGY7TQxmPg';


_header_file=$(mktemp);
_content_file=$(mktemp);


function _curl {
    curl \
        --location \
        --dump-header ${_header_file} \
        --output ${_content_file} \
        --silent \
        --show-error \
        --trace - \
        "$@";
}


function _cleanup {
    rm ${_header_file};
    rm ${_content_file};
}


function _press_key {
    read -n 1 -s -r -p "Press any key to continue";
}


function _step_title {
    echo;
    echo;
    echo "$1";
    echo;
}


function _report_env {
    echo api_host = ${api_host};
    echo user_agent = ${user_agent};
    echo client_id = ${client_id};
    echo client_secret = ${client_secret};
    echo credentials_encoded = ${credentials_encoded};
    echo access_token = ${access_token};
    echo file_type = ${file_type};
    echo generated_date = ${generated_date};
    echo download_url = ${download_url};

    echo;
}


function _report_step {
    # cat ${_trace_file};
    cat ${_header_file};
    cat ${_content_file};
    echo;
    echo;
    _report_env;
    _press_key;
}


function _json_get {
    local value=$(jq "$@" ${_content_file});
    echo ${value} | tr -d \";
}


# Code here

_step_title '1. Login';

credentials_encoded=$(echo -n "${client_id}:${client_secret}" | base64);
_body="grant_type=client_credentials&client_id=${client_id}&client_secret=${client_secret}"

_curl \
    -X POST \
    "https://accounts.pracuj.pl/connect/token" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -H "Authorization: Basic ${credentials_encoded}" \
    --data "grant_type=client_credentials&client_id=${client_id}&client_secret=${client_secret}" \
    ;

access_token=$(_json_get '.access_token');

_report_step;



_step_title '2. Acquire link to file';

function yesterday {
    \date +"%Y-%m-%d"
}

file_type='all';
generated_date=$(yesterday);

_curl \
    -X GET \
    "https://offers-feed.pracuj.pl/api/files?fileType=${file_type}&generatedDate=${generated_date}" \
    -H "Authorization: Bearer ${access_token}" \
    ;

download_url=$(_json_get '.url');

_report_step;



_step_title '3. Download the file';

# # no body for this step

_curl \
"${download_url}" \
-X GET \
;

cat ${_header_file};
_report_env;
_press_key;



# _cleanup;
