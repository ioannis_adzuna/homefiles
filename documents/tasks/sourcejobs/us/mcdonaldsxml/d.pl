use Adzuna::DSL::YAMLDeploy;

build_split(
    description_format => 'mcdonalds.jibeapply.com: XML all ads %s',
    pool_name => 'mcdonalds-title-us',
    attribute => 'title',
    conf => 'scraper/us/www/job/mcdonaldsxml.conf',
    split_values => ['Manager|Management'],
    # split_regex_map => sub {

    #     my $label = $_[0];
    #     $initials = quotemeta($label);
    #     $initials =~ s/\\\s/\\s*/g;
    #     return "qr~\b$initials\b~s";
    # },
    split_label_map => sub{ " - '$_[0]'" },
    context => 'jobs_US',
    sales => 'Direct',
    category => 'unknown',
    url => 'https://feeds.jibepost.com/mcdonalds/mchire-mcopco.xml',
);


output;
