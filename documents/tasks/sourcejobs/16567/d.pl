#!/usr/bin/env perl

use Adzuna::DSL::YAMLDeploy;

site(
    logo => 'images/fr/jobs/partners/logo_formapostemidiatlantique.png',
    name => 'Formaposte Midi Atlantique',
    proxies => 'localhost',
);

my %common = (
    category => 'logistics-warehouse-jobs',
    conf => 'scraper/fr/www/job/formapostemidiatlantique.conf',
    context => 'jobs_FR',
    frequency_minutes => '360',
    sales => 'Direct',

    attribute => 'cpc',
    split_values => [0, 0.10, 0.12, currency_range(0.15, 1.00, 0.05)],
);

build_currency_split(
    %common,
    description_format => 'jobfeed.goldenbees.fr: Facteurs XML all ads %s',
    url => 'https://jobfeed.goldenbees.fr/?t=2ns6zB3Xq34H4LyD9804a8d6218a63771473c46e3bee635d9ef244a9&cid=537&format=adzuna',
    pool_name => 'formapostemidiatlantique-facteurs-cpc-nl',
);

build_currency_split(
    %common,
    description_format => 'jobfeed.goldenbees.fr: Charge de Clientele XML all ads %s',
    url => 'https://jobfeed.goldenbees.fr/?t=2ns6zB3Xq34H4LyD9804a8d6218a63771473c46e3bee635d9ef244a9&cid=538&format=adzuna',
    pool_name => 'formapostemidiatlantique-chargedeclientele-cpc-nl',
);

build_currency_split(
    %common,
    description_format => 'jobfeed.goldenbees.fr: Autres XML all ads %s',
    url => 'https://jobfeed.goldenbees.fr/?t=2ns6zB3Xq34H4LyD9804a8d6218a63771473c46e3bee635d9ef244a9&cid=539&format=adzuna',
    pool_name => 'formapostemidiatlantique-autres-cpc-nl',
);

output;
