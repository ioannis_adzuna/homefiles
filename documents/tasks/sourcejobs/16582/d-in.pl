#!/usr/bin/env perl

use Adzuna::DSL::YAMLDeploy;

site (
    logo => 'images/shared/jobs/partners/logo_aviahire.png',
    name => 'Aviahire',
    proxies => 'localhost',
);

source (
    category => 'unknown',
    conf => 'scraper/in/www/job/aviahire.conf',
    context => 'jobs_IN',
    description => 'jobs.aviahire.com: XML all ads',
    frequency_minutes => '1440',
    url => 'https://jobs.aviahire.com/rss/adzuna',
    filter => { tokenizer => { country => 'adzuna:location:in' } },
    sales => 'ATS/Multiposter',
);
output;
