#!/usr/bin/env perl

use Adzuna::DSL::YAMLDeploy;

site (
    logo => 'images/shared/jobs/partners/logo_aviahire.png',
    name => 'Aviahire',
    proxies => 'localhost',
);

source (
    category => 'unknown',
    conf => 'scraper/us/www/job/aviahire.conf',
    context => 'jobs_US',
    description => 'jobs.aviahire.com: XML all ads',
    frequency_minutes => '1440',
    url => 'https://jobs.aviahire.com/rss/adzuna',
    filter => { tokenizer => { country => 'adzuna:location:us' } },
    sales => 'ATS/Multiposter',
);
output;
