#!/usr/bin/env perl

use Adzuna::DSL::YAMLDeploy;

site (
    logo => 'images/shared/jobs/partners/logo_aviahire.png',
    name => 'Aviahire',
    proxies => 'localhost',
);

source (
    category => 'unknown',
    conf => 'scraper/ca/www/job/aviahire.conf',
    context => 'jobs_CA',
    description => 'jobs.aviahire.com: XML all ads',
    frequency_minutes => '1440',
    url => 'https://jobs.aviahire.com/rss/adzuna',
    filter => { tokenizer => { country => 'adzuna:location:ca' } },
    sales => 'ATS/Multiposter',
);
output;
